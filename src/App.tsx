import React from 'react'
import { connect } from "react-redux"

import { AuthState } from "./redux/reducers/auth";
import { MultiplayerState } from "./redux/reducers/multiplayer" // TODO: Ask someone how we can group those into a combined reducer type
import { LoginState } from "./auth"

import LoginScreen from "./components/screens/LoginScreen"
import MainScreen from "./components/screens/MainScreen"

interface AppProps {
  auth: AuthState,
  multiplayer: MultiplayerState
}

const App: React.FC<AppProps> = ({ auth, multiplayer }) => {
  if (auth.loginState !== LoginState.LOGGED_IN) {
    return <LoginScreen />
  }

  return <MainScreen />
}

function mapStateToProps({auth, multiplayer }: {auth: AuthState, multiplayer: MultiplayerState}) { return {auth, multiplayer} }

export default connect(mapStateToProps)(App);
