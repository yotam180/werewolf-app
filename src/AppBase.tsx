import React from "react"
import { Provider } from "react-redux"

import { store } from "./redux/store"
import App from "./App";

import "./assets/global.css"

const AppBase: React.FC = () => (
    <Provider store={store}>
        <App />
    </Provider>
)

export default AppBase
