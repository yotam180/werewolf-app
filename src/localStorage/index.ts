export const WEREWOLF_NICKNAME = "WEREWOLF_NICKNAME";

export function get(index: string) {
    return localStorage.getItem(index)
}

export function set(index: string, value: string) {
    localStorage.setItem(index, value)
}
