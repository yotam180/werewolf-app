export interface Role {
    name: string,
    color: string,
}

export const Roles: { [code: string] : Role} = {
    "VILLAGER": { name: "Villager", color: "#306b34"},
    "HEALER": { name: "Healer", color: "#3a8a3f"},
    "BODYGUARD": { name: "Bodyguard", color: "#3a8a3f"},
    "FORTUNE_TELLER": { name: "Fortune Teller", color: "#316951"},
    "PRIEST": { name: "Priest", color: "#306b34"},
    "INVESTIGATOR": { name: "Investigator", color: "#316951"},
    "SPY": { name: "Spy", color: "#316951"},
    "VETERAN": { name: "Veteran", color: "#306b34"},
    "RANDOM_TOWN": { name: "♦ Random ♦", color: "#306b34"},
    "RANDOM_INV": { name: "♦ Investigative ♦", color: "#316951"},
    "RANDOM_ATTACK": { name: "♦ Attack ♦", color: "#306b34"},
    "RANDOM_DEFENSE": { name: "♦ Defense ♦", color: "#3a8a3f"},
    "WEREWOLF": { name: "Werewolf", color: "#d64933"},
    "WOLF_SEER": { name: "Wolf Seer", color: "#d64933"},
    "WITCH": { name: "Witch", color: "#662e9b"},
    "CREEPY_GIRL": { name: "Creepy Girl", color: "#662e9b"},
    "ARSONIST": { name: "Arsonist", color: "#de7a00"},
    "JESTER": { name: "Jester", color: "#43bccd"},
    "FOOL": { name: "Fool", color: "#43bccd"},
    "RANDOM_EVIL": { name: "♦ Evil ♦", color: "#d2e600"},
    "RANDOM": { name: "♦ Random ♦", color: "#000000"},
}
