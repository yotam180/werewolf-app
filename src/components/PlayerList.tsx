import React from "react"
import styled from "styled-components"

import colors from "../assets/colors.json"

export interface Player {
    nickname: string,
    data?: string,
    dataColor?: string,
}

interface Props {
    players: Player[]
}

const PlayerList: React.FC<Props> = ({ players }) => (
    <div style={{maxWidth: 800, alignSelf: "center", width: "100%"}}>
        { players.map((p, i) => <CharacterBox key={i} player={p} />) }
    </div>
)

export default PlayerList

interface CharacterBoxProps {
    onClick?: (() => null),
    player: Player,
}

const CharacterBox: React.FC<CharacterBoxProps> = ({
    player,
    onClick
}) => (
    <CharacterOutline onClick={onClick}>
        <CharacterFrame>
            <img
                style={{width: "100%", height: "100%", margin: "auto"}}
                src={`https://api.adorable.io/avatars/285/${player.nickname}.png`}
            />
            <div style={{
                position: "absolute",
                bottom: 0,
                right: 0,
                left: 0,
                textAlign: "center",
                backgroundColor: "#aaaaaaaa",
                color: "black",
                fontWeight: "bold",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                fontSize: "smaller"
            }}>
                <div style={{ alignSelf: "center", marginBottom: -3 }}>{player.nickname}</div>
            </div>

            {player.data && (
                <div
                style={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    padding: 4,
                    borderRadius: "0 0 10px 0",
                    backgroundColor: player.dataColor || colors.main,
                    fontFamily: "Sans Serif",
                    fontSize: "smaller"
                }}
                >
                {player.data?.length < 2 && <b>&nbsp;</b>}
                {player.data}
                {player.data?.length < 2 && <b>&nbsp;</b>}
                </div>
            )}

        </CharacterFrame>
    </CharacterOutline>
)

const CharacterOutline = styled.div`
  vertical-align: top;
  min-width: 10vw;
  max-width: 25vw;
  width: 110px;
  min-height: 10vw;
  max-height: 25vw;
  height: 110px;
  color: white;
  display: inline-flex;
  box-sizing: border-box;
  padding: 2px;
`;

const CharacterFrame = styled.div`
  position: relative;
  display: flex;
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  border-radius: 10px;
`;

const SelectionBorder = styled.div`
  box-sizing: border-box;
  border: 4px solid ${props => props.color};
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  position: absolute;
  border-radius: 10px;
`;
