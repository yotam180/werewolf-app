import React from "react"
import * as Colyseus from "colyseus.js"

import Header, { Props as HeaderProps } from "./Header"
import { Button } from "../common"
import { client } from "../../multiplayer"
import { SignalState } from "../../game/signal"

export interface Props {
    room: Colyseus.Room
}

function stripRoomId(roomId: string) {
    return parseInt(roomId).toString()
}

function makeReadyBtn(room: Colyseus.Room) {
    console.log(room.state.players)
    if (room.state.players[room.sessionId]?.signalState === SignalState.TRUE) {
        return <Button narrow color="#444444" onClick={() => room.send("not_ready")}>Not ready</Button>
    }
    else {
        return <Button narrow color="green" onClick={() => room.send("ready")}>Ready</Button>
    }
}

const LobbyHeader: React.FC<Props & HeaderProps> = ({room}) => (
    <Header 
        center={
            <span style={{fontSize: "larger", fontWeight: "bold"}}>Party #{ stripRoomId(room.id) } </span>
        }

        right={ makeReadyBtn(room) }
    />
)

export default LobbyHeader
