import React from "react";

export interface Props {
  left?: any;
  right?: any;
  center?: any;
}

const Header: React.FC<Props> = ({ left, center, right }) => (
  <div
    style={{
      borderBottom: "1px solid #ccc",
      height: 40,
      maxHeight: "8%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    }}
  >
    &nbsp;
    <span style={{ flex: 1 }}>{left}</span>
    <span style={{ flex: 1, textAlign: "center" }}>{center}</span>
    <span style={{ flex: 1, textAlign: "right" }}>{right}</span>
    &nbsp;
  </div>
)

export default Header