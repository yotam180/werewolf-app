import React from "react"
import * as Colyseus from "colyseus.js"

import Header from "./Header"

interface Props {
    room: Colyseus.Room
}

const DefaultHeader: React.FC<Props> = ({ room }) => (
    <Header center="Hello world"/>
)

export default DefaultHeader;
