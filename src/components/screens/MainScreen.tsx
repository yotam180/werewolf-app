import React from "react"
import { connect } from "react-redux"

import { RootState } from "../../redux/reducers"
import { RoomState } from "../../multiplayer";
import EntryScreen from "./EntryScreen";
import GameScreen from "./GameScreen";

type Props = RootState;

const MainScreen: React.FC<Props> = ({ multiplayer }) => {
    if (multiplayer.roomState === RoomState.NOT_IN_ROOM) {
        return <EntryScreen multiplayer={multiplayer} />
    }

    if (multiplayer.roomState === RoomState.INSIDE_ROOM && multiplayer.room) {
        return <GameScreen room={multiplayer.room} />
    }

    return <div>Undefined state</div>
}

const mapStateToProps = (state: RootState) => state

export default connect(mapStateToProps)(MainScreen)
