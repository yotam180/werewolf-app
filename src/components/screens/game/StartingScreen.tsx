import React from "react"
import * as Colyseus from "colyseus.js"
import { FlexContainer, Logo } from "../../common"

interface Props {
    room: Colyseus.Room
}

export const StartingScreen: React.FC<Props> = ({ room }) => {
    return (
       <div style={{alignSelf: "center", textAlign: "center"}}>
           <br/>
           <Logo overrideText="Prepare"></Logo>
           <br/>
           Game is starting...
       </div>
    )
}
