import React from "react"
import * as Colyseus from "colyseus.js"

import PlayerList, { Player } from "../../PlayerList"
import { SignalState } from "../../../game/signal"
import Tabs from "../../Tabs"
import { Roles, Role } from "../../../game/roles"
import { Button } from "../../common"

interface Props {
    room: Colyseus.Room
}

const PlayerListScreen: React.FC<Props> = ({ room }) => {
    function createPlayersList(): Player[] {
        if (!room.state) {
            return [];
        }
    
        return Object.values(room.state.players).map((player: any) => ({ // TODO: Should we really define Player as any here?
            nickname: player.nickname,
            dataColor: "green",
            data: player.signalState === SignalState.TRUE ? "ready" : undefined,
        }));
    }
    
    return (
        <>
            <PlayerList players={ createPlayersList() } />
            <div style={{flex: 1}}>&nbsp;</div>
        </>
    )
}

const GameSettingsScreen: React.FC<Props> = ({ room }) => {

    function addRole(role: string) {
        room.send("set_roles", {"roles": room.state.rolesBank.concat([role])})
    }

    function removeRole(index: number) {
        const roles = [...room.state.rolesBank]
        roles.splice(index, 1)
        room.send("set_roles", {roles})
    }

    function roleButton(key: any, role: Role, onClick: () => void): any {
        return (
            <div key={key}>
            <Button narrow color={role.color} style={{marginBottom: 4, width: "100%"}} onClick={onClick}>{role.name}</Button>
            </div>
        )
    }

    return (
        <>
        <div style={{flex: 0.8, textAlign: "center", display: "flex", flexDirection: "column"}}>
            <span style={{fontSize: "larger"}}>Build up your town:</span>
            <div style={{columnCount: 3, columnFill: "auto", flex: 1}}>
                { Object.keys(Roles).map(role => roleButton(role, Roles[role], () => addRole(role))) }
            </div>
        </div>
        <div style={{flex: 1, textAlign: "center", display: "flex", flexDirection: "column"}}>
            <span style={{fontSize: "larger"}}>Town roles:</span>
            <div style={{columnCount: 3, columnFill: "auto", flex: 1}}>
                { room.state.rolesBank.map((role: string, index: number) => roleButton(index, Roles[role], () => removeRole(index))) }
            </div>
        </div>
        </>
    )
}

const WaitingScreen: React.FC<Props> = ({ room }) => {

    const [tab, setTab] = React.useState(0)

    const tabs: { [key: number]: any } = {
        0: PlayerListScreen,
        1: GameSettingsScreen
    }
    
    const CurrentTab = tabs[tab]

    return (
        <>
            <CurrentTab room={room} />
            <Tabs tabs={[
                { content: "Game Screen", onClick: () => setTab(0) },
                { content: "Settings", onClick: () => setTab(1) }
            ]} />
        </>
    )
}

export default WaitingScreen
