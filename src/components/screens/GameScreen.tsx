import React from "react"
import * as Colyseus from "colyseus.js"

import LobbyHeader from "../headers/LobbyHeader"
import DefaultHeader from "../headers/DefaultHeader"

import WaitingScreen from "./game/WaitingScreen"
import { StartingScreen } from "./game/StartingScreen"
import { FlexContainer } from "../common"

interface Props {
    room: Colyseus.Room,
}

function getHeader(phase: string) {
    switch (phase) {
        case "WAITING":
            return LobbyHeader;
        default:
            return DefaultHeader;
    }
}

function getScreen(phase: string) {
    return StartingScreen;
    switch (phase) {
        case "WAITING":
            return WaitingScreen;
        case "STARTING":
            return StartingScreen;
        default:
            return null;
    }
}

const GameScreen: React.FC<Props> = ({ room }) => {

    const [,forceUpdate] = React.useState(0)

    React.useEffect(() => {
        room.onStateChange(() => forceUpdate(Math.random()))
    }, [])

    if (!room) {
        return <div>nope</div>;
    }

    const Header = getHeader(room.state.phase)
    const Screen = getScreen(room.state.phase)

    return (
        <>
            <FlexContainer.Full>
            { Header && <Header room={room}/> }
                { Screen && <Screen room={room} /> }
            </FlexContainer.Full>
        </>
    )
}

export default GameScreen
