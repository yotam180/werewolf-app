import React from "react"
import { connect } from "react-redux"

import { Button, InlineImage, FlexContainer, Logo, Overlay } from "../common"
import { loginWithGoogle } from "../../auth"
import { RootState } from "../../redux/reducers"
import { LoginState } from "../../redux/reducers/auth"

const IMAGE_LINK = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1200px-Google_%22G%22_Logo.svg.png"

type Props = RootState;

const LoginScreen: React.FC<Props> = ({ auth }) => {
    
    async function onLogin() {
        await loginWithGoogle();
    }

    function generateOverlay() {
        return (
            <Overlay style={{backgroundColor: "#00000099", color: "white"}}>
                <h2>Connecting...</h2>
            </Overlay>
        )
    }

    return (
        <FlexContainer.Full>

            { auth.loginState === LoginState.NOT_INITIALIZED && generateOverlay() }

            <Logo style={{marginTop: "20vh", marginBottom: 50}} />
            
            <div style={{textAlign: "center"}}>
                <Button onClick={onLogin} color="while" textColor="gray" style={{border: ".5px solid #ccc", borderRadius: 3, fontSize: "larger"}}>
                    &nbsp;Login with&nbsp;
                    <InlineImage src={IMAGE_LINK}></InlineImage>&nbsp;
                </Button>
            </div>
        </FlexContainer.Full>
    )
}

const mapStateToProps = ( state: RootState ) => state

export default connect(mapStateToProps)(LoginScreen)
