import React from "react"

import Textbox from "../common/Textbox"
import { FlexContainer, Logo, Button } from "../common"
import { joinRoom, createRoom } from "../../multiplayer"
import { MultiplayerState } from "../../redux/reducers/multiplayer"

interface Props {
    multiplayer: MultiplayerState
}

function processRoomId(roomId: string) {
    return roomId.toString().padStart(9, "0")
}

const EntryScreen: React.FC<Props> = ({ multiplayer }) => {

    const [roomId, setRoomId] = React.useState("")

    return (
        <FlexContainer.Full>
            <Logo 
                style={{
                    marginTop: 60,
                }}
                overrideText="Werewolf" />

            <div style={{
                textAlign: "center",
                alignSelf: "center",
                marginTop: 30,
                width: 250,
                maxWidth: "80%",
            }}>
                
                <div style={{display: "flex", flexDirection: "column"}}>
                <Textbox
                    placeholder="Party number"
                    style={{
                        textAlign: "center",
                        letterSpacing: 2, // TODO: Solve later
                    }}
                    type="number"
                    value={roomId}
                    onChange={e => setRoomId(e.target.value)}
                />

                <Button onClick={() => joinRoom(processRoomId(roomId))}>Join a party</Button>
                { multiplayer.error &&  <span style={{color: "red"}}>{multiplayer.error}</span> /* TODO: Render a real error object */}

                <div style={{display: "block", height: 50}}>&nbsp;</div>
                
                <Button onClick={createRoom} color="#444">Create a party</Button>
                
                </div>

            </div>

        </FlexContainer.Full>
    )
}

export default EntryScreen
