import styled from "styled-components"

import colors from "../../assets/colors.json"

interface ButtonProps {
    color?: string,
    textColor?: string,
    narrow?: boolean
}

console.log("Main: ", colors.main)

const Button = styled.div<ButtonProps>`

    background-color: ${props => props.color || colors.main};
    padding: ${props => props.narrow ? 4 : 8}px;
    padding-top: ${props => props.narrow ? 5 : 10}px;
    color: ${props => props.textColor || "white"};
    cursor: pointer;
    display: inline-block;
    flex-grow: 0;
    text-align: center;

    user-select: none;
    -moz-user-select: none;
    -khtml-user-select: none;
    -webkit-user-select: none;
    -o-user-select: none;
`;

export default Button;
