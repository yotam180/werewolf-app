import styled from "styled-components"

const Full = styled.div`
    display: flex;
    flex-direction: column;
    justifyContent: center;
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
`

const Fluid = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    flex-grow: 1;
`

export { Fluid, Full }
