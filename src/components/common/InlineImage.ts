import styled from "styled-components"

interface Props {
    src: string
}

const InlineImage = styled.img.attrs<Props>(props => ({
    src: props.src
}))<Props>`
    display: inline-block;
    height: 1rem;
    vertical-align: middle;
`;

export default InlineImage
