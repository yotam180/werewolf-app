import styled from "styled-components"

import colors from "../../assets/colors.json"

interface Props {
    type?: string
}

const Textbox = styled.input.attrs<Props>(props => ({
    type: props.type || "text"
}))<Props>`
    line-height: 2em;
    font-size: 18px;
    vertical-align: middle;
    font-weight: 700;
    font-family: Normal;
    border: 3px solid ${colors.gray};
    padding-left: 5px;
    padding-right: 5px;
    margin-bottom: 8px;
    outline: none;
`

export default Textbox
