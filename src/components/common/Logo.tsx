import React from "react";

import colors from "../../assets/colors.json"

interface Props {
  fontSize?: number;
  style?: any;
  textOnly?: boolean;
  overrideText?: string;
  [option: string]: any;
}

const styles = {
  span: {
    fontFamily: "Werewolf",
    display: "inline-block",
    marginBottom: -10,
    color: colors.main,
  },
  container: {
    textAlign: "center",
    display: "inline-block",
  },
};

const Logo: React.FC<Props> = ({
  fontSize,
  style,
  textOnly,
  overrideText,
  ...props
}) => {
  return (
    <div style={{ ...styles.container, ...style }} {...props}>
      <span style={{ ...styles.span, fontSize: fontSize || 48 }}>
        {overrideText || "Werewolf"}
      </span>
      <br />
      {!textOnly && (
        <img
          src={require("../../assets/images/ww.jpg")}
          style={{ height: 150, maxHeight: "20vh" }}
          alt="Werewolf Logo"
        />
      )}
    </div>
  );
};

export default Logo;
