import React from "react"
import styled from "styled-components"

import colors from "../assets/colors.json"

interface TabInfo {
    content: any,
    onClick: () => void
}

interface Props {
    tabs: TabInfo[]
}

const Tabs: React.FC<Props> = ({ tabs }) => {
    return (
        <BottomBar>
            { tabs.map((t, i) => <BottomTab key={i} onClick={t.onClick}>{t.content}</BottomTab>) }
        </BottomBar>
    )
}

const BottomBar = styled.div`
    height: 48px;
    width: 100%;
    background-color: ${colors.dark};
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;
`;

const BottomTab = styled.div`
    flex: 1;
    text-align: center;
`;

export default Tabs