import * as firebase from "firebase"

import config from "../firebase"
import { dispatch } from "../redux/store"
import * as LoginActions from "../redux/actions/auth"
import * as LocalStorage from "../localStorage"

export { LoginState } from "../redux/reducers/auth"

function randomName() {
    return "Steve Marcus"
}

firebase.initializeApp(config)
firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)

firebase.auth().onAuthStateChanged(user => {
    console.log("authStateChange", user)
    if (!user) {
        dispatch(LoginActions.logout())
        return
    }

    dispatch(LoginActions.login(user))
    if (!LocalStorage.get(LocalStorage.WEREWOLF_NICKNAME)) {
        LocalStorage.set(LocalStorage.WEREWOLF_NICKNAME, user.displayName || randomName())
    }
})

export async function loginWithGoogle() {
    try {
        const data = await firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider())
        if (data.user) {
            dispatch(LoginActions.login(data.user))
        }
    }
    catch (error) {
        alert("Error authenticating with firebase" +  error.toString())
        dispatch(LoginActions.loginFailed(error)) // We assume it's a firebase.auth.Error variable
    }
    finally {}
}

export async function logout() {
    await firebase.auth().signOut()
    dispatch(LoginActions.logout())
}
(window as any).logout = logout; // TODO: remove
