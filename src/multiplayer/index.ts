import * as Colyseus from "colyseus.js";

import { dispatch, store } from "../redux/store"
import * as Actions from "../redux/actions/multiplayer"
import * as LocalStorage from "../localStorage"

export { RoomState } from "../redux/reducers/multiplayer"

const REMOTE = `ws://${window.location.hostname}:2567`

export let client: Colyseus.Client = new Colyseus.Client(REMOTE)

function initRoom(room: Colyseus.Room) {
    room.onLeave(() => {
        dispatch(Actions.leaveRoom())
    })
}

async function getToken() {
    const user = store.getState().auth.user;
    if (!user) {
        return ""
    }

    return await user.getIdToken()
}

async function makeOptions() {
    return {
        token: await getToken(),
        info: {
            nickname: LocalStorage.get(LocalStorage.WEREWOLF_NICKNAME)
        }
    }
}

export async function joinRoom(roomId: string) {
    try
    {
        const room = await client.joinById(roomId, await makeOptions())
        initRoom(room)
        dispatch(Actions.joinedRoom(room))
    }
    catch (error)
    {
        dispatch(Actions.roomJoinError(error.toString()))
    }
    finally {} // Why?
}

export async function createRoom() {
    try
    {
        const room = await client.create("werewolf_room", await makeOptions())
        initRoom(room)
        dispatch(Actions.joinedRoom(room));
    }
    catch (error) // TODO: Is this really a string?
    {
        dispatch(Actions.roomJoinError(error.toString()))
    }
}

(window as any).createRoom = createRoom;
