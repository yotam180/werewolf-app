import { combineReducers } from "redux"

import auth, { AuthState } from "./auth"
import multiplayer, { MultiplayerState } from "./multiplayer"

const combined = {
    auth,
    multiplayer
}

export type MainReducer = typeof combined
export default combineReducers(combined)

export interface RootState {
    auth: AuthState,
    multiplayer: MultiplayerState,
}
