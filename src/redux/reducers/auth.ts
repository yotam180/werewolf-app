import firebase from "firebase"
import { Action } from "redux"

import * as Actions from "../actions"

export interface AuthAction extends Action<string> {
    user?: firebase.User,
    error?: firebase.auth.Error | undefined,
}

export enum LoginState {
    NOT_INITIALIZED,
    LOGGED_IN,
    LOGGED_OUT,
    LOGIN_ERROR
}

export interface AuthState {
    loginState: LoginState,
    user?: firebase.User,
    error?: firebase.auth.Error,
}

const initialState: AuthState = {
    loginState: LoginState.NOT_INITIALIZED,
    user: undefined,
    error: undefined,
}

export default function reducer(state: AuthState = initialState, action: AuthAction): AuthState {
    switch (action.type) {
    case Actions.LOGGED_IN:
        return { loginState: LoginState.LOGGED_IN, user: action.user }

    case Actions.LOGGED_OUT:
        return { loginState: LoginState.LOGGED_OUT }

    case Actions.AUTH_ERROR:
        return { loginState: LoginState.LOGIN_ERROR, error: action.error }
        
    default:
        return state
    }
}
