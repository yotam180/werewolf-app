import * as Colyseus from "colyseus.js"
import { Action } from "redux"

import * as Actions from "../actions"

export interface MultiplayerAction extends Action<string> {
    room?: Colyseus.Room,
    error?: string,
}

export enum RoomState {
    NOT_IN_ROOM,
    INSIDE_ROOM,
    JOINING_ROOM,
}

export interface MultiplayerState {
    roomState: RoomState,
    room?: Colyseus.Room, // Must be defined if roomState is defined, is there a better way to represent this?
    error?: string,
}

const initialState: MultiplayerState = {
    roomState: RoomState.NOT_IN_ROOM,
    room: undefined,
    error: undefined,
}

export default function reducer(state: MultiplayerState = initialState, action: MultiplayerAction) {
    switch (action.type) {
    case Actions.MP_START_JOINING_ROOM:
        return { roomState: RoomState.JOINING_ROOM }

    case Actions.MP_ROOM_LEFT:
        return { roomState: RoomState.NOT_IN_ROOM }

    case Actions.MP_ROOM_JOINED:
        return { roomState: RoomState.INSIDE_ROOM, room: action.room }

    case Actions.MP_ROOM_JOIN_ERROR:
        return { roomState: RoomState.NOT_IN_ROOM, error: action.error }

    default:
        return state;
    }
}