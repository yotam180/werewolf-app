import * as Colyseus from "colyseus.js"

import * as Actions from "./index"

export function startJoiningRoom() {
    return {
        type: Actions.MP_START_JOINING_ROOM
    }
}

export function joinedRoom(room: Colyseus.Room) {
    (window as any).room = room; // TODO: Remove this (debug)
    return {
        type: Actions.MP_ROOM_JOINED,
        room,
    }
}

export function roomJoinError(error: string) {
    return {
        type: Actions.MP_ROOM_JOIN_ERROR,
        error
    }
}

export function leaveRoom() {
    return {
        type: Actions.MP_ROOM_LEFT
    }
}
