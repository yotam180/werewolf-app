import firebase from "firebase";

import * as Actions from "./index"

export function login(user: firebase.User) {
    return {
        type: Actions.LOGGED_IN,
        user,
    }
}

export function logout() {
    return {
        type: Actions.LOGGED_OUT,
    }
}

export function loginFailed(error: firebase.auth.Error) {
    return {
        type: Actions.AUTH_ERROR,
        error
    }
}
